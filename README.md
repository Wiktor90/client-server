**Aplikacja Client - Server**


Projekt opierający się na socketach, prezentujący komunikacje clienta z serverem.
W projekcie obsłużone jest zarządzanie użytkownikami, so dodane prawa userów oraz możliwość wysyłania wiadomości.
Do komunikacji z bazą użyty został moduł psycopg2


**Informacje:**
1. Aplikacja działa na localhost
2. Baza Danych - PostgreSQL
3. Demo : https://www.youtube.com/watch?v=aTCn1eafFD4
