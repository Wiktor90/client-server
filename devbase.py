import sqlite3

from tabulate import tabulate


class SqliteDB:
    def __init__(self):
        self.connection = sqlite3.connect("CS_sqlite.db")

    def execute_query(self, query: str, params: tuple):
        """for INSERT, UPDATE, DELETE with params"""

        with self.connection as conn:
            cursor = conn.cursor()
            try:
                cursor.execute(query, params)
            except Exception as e:
                raise Exception(e)
            conn.commit()

    def execute_select(self, query: str, params=""):
        """for SELECT query with no params"""

        with self.connection as conn:
            cursor = conn.cursor()
            try:
                cursor.execute(query, params)
            except Exception as e:
                raise Exception(e)

            records = cursor.fetchall()
            headers = [head[0] for head in cursor.description]
            return records, headers

    def show_data(self, query):
        with self.connection:
            data = self.execute_select(query)
            table = tabulate(data[0], data[1], tablefmt="psql")
            return table

    def get_user(self, username):
        query = "SELECT * FROM users WHERE username = ?"
        with self.connection:
            cursor = self.connection.cursor()
            cursor.execute(query, (username,))
            record = cursor.fetchone()
            return record

    def add_user_to_DB(self, object_data):
        if self.get_user(object_data["username"]) is None:
            query = """INSERT INTO users (username, password, rights)
                    VALUES (?,?,?)"""
            data_to_insert = (
                object_data["username"],
                object_data["password"],
                object_data["rights"],
            )
            self.execute_query(query, data_to_insert)
            return True
        return None

    def delete_user_from_DB(self, username):
        query = "DELETE FROM users WHERE username = ?"
        params = (username,)
        if self.get_user(username) is not None:
            self.execute_query(query, params)
            return True
        return None

    def password_reset(self, username, new_pass):
        query = "UPDATE users SET password = ? WHERE username = ?"
        params = (new_pass, username)
        if self.get_user(username) is not None:
            self.execute_query(query, params)
            return True
        return None

    def check_user_credentials(self, username, password):
        """checking user credentials and return user rights level"""

        record = self.get_user(username)
        if record is not None and record[2] == password:
            return record[3]
        return None

    def send_direct_msg(self, receiver, sender, text):
        if self.get_user(receiver) is not None:
            query = """INSERT INTO msgbox (sender, receiver, msg_text)
                    VALUES (?, ?, ?)"""
            params = (sender, receiver, text[:255].strip())
            self.execute_query(query, params)
            return "OK"
        return None

    def read_msg_box(self, username):
        query = """SELECT sender, msg_text FROM msgbox
        WHERE receiver = ?"""
        params = (username,)
        data = self.execute_select(query, params)
        records = data[0]
        msg = {}
        for tup in records:
            msg[tup[0]] = tup[1]
        return msg

    def clear_msg_box(self, username):
        if self.get_user(username) is not None:
            query = "DELETE FROM msgbox WHERE receiver = ?"
            params = (username,)
            self.execute_query(query, params)
            return True
        return None
