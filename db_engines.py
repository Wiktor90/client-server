import psycopg2 as ps2
import sqlite3


class PostgresDB:
    def __init__(self, db_user, password, host, port, database):
        self.db_user = db_user
        self.password = password
        self.host = host
        self.port = port
        self.database = database
        self.connection = ps2.connect(
            user=self.db_user,
            password=self.password,
            host=self.host,
            port=self.port,
            database=self.database,
        )


class SqliteDB:
    def __init__(self):
        self.connection = sqlite3.connect("CS_sqlite.db")

    def execute_query(self, query: str, params: tuple):
        """for INSERT, UPDATE, DELETE with params"""

        with self.connection as conn:
            cursor = conn.cursor()
            try:
                cursor.execute(query, params)
            except Exception as e:
                raise Exception(e)
            conn.commit()

    def execute_select(self, query: str):
        """for SELECT query with no params"""

        with self.connection as conn:
            cursor = conn.cursor()
            try:
                cursor.execute(query)
            except Exception as e:
                raise Exception(e)

            records = cursor.fetchall()
            headers = [head[0] for head in cursor.description]
            return records, headers
