from database import DataBase
from devbase import SqliteDB
from configparser import ConfigParser


def initialize_PostgresDB():
    print("PROD-db-postgres")
    config = ConfigParser()
    config.read("config.ini")

    db = DataBase(
        config["DATABASE"]["db_user"],
        config["DATABASE"]["password"],
        config["DATABASE"]["host"],
        config["DATABASE"]["port"],
        config["DATABASE"]["database"],
    )
    return db


def initialize_SqliteDB():
    print("DEV-db-sqlite3")
    db = SqliteDB()
    return db


def select_db_engine() -> object:
    engines = {"postgres": initialize_PostgresDB, "sqlite": initialize_SqliteDB}

    select = input(
        """
    Check DE engine (type 1 or 2):
    1 - Production (PostgresSQL)
    2 - Dev (SQLite)
    : """
    )

    if select == "1":
        return engines["postgres"]()
    return engines["sqlite"]()
