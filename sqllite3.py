import sqlite3



def query_sqlite_db(query: list):
    conn = sqlite3.connect("CS_sqlite.db")
    c = conn.cursor()

    for q in query:
        c.execute(q)

    conn.commit()
    conn.close()


# --- drop all table ---
# query = ["""DROP TABLE msgbox""", """DROP TABLE users"""]
# query_sqlite_db(query)

# --- initial SQLite db - running one time ----
query = [
    """
CREATE TABLE users (
user_id integer PRIMARY KEY,
username text UNIQUE NOT NULL,
password text NOT NULL,
rights text NOT NULL,
created TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
""",
    """
CREATE TABLE msgbox (
msg_id integer PRIMARY KEY,
sender text NOT NULL,
receiver text NOT NULL REFERENCES users(username) ON DELETE CASCADE,
msg_text text,
sent_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
)
""",
]

query_sqlite_db(query)


# --- filling basic data ---
query = [
    """
INSERT INTO users (username, password, rights) VALUES
('Wiktor', 'admin', 'ADMIN'),
('Daniel', 'test', 'USER'),
('Marek', 'test', 'USER')
""",
    """
INSERT INTO msgbox (sender, receiver, msg_text) VALUES
('Wiktor', 'Daniel', 'testowa msg1'),
('Wiktor', 'Marek', 'testowa msg2'),
('Daniel', 'Daniel', 'testowa msg3'),
('Marek', 'Wiktor', 'testowa msg4')
""",
]

# query_sqlite_db(query)
